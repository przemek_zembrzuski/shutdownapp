# ShutdownApp #

ShutdownApp is a multi platform app for shutdown your computer now or delay it in time.

### Installation & running ###

	git clone https://przemek_zembrzuski@bitbucket.org/przemek_zembrzuski/shutdownapp.git shutdownapp
	cd shutdownapp
	npm i
	npm start
