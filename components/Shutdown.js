const os = require('os');
const { exec } = require('child_process');
class Shutdown{
    system(){
        return os.platform()
    }
    prepareCommand(time){
        const commands = {
            linux:`shutdown -h ${time}`,
            win32:`shutdown -s -t ${time}`
        }
        return Object.keys(commands).filter(key=>key === this.system())
                             .map(key=>commands[key])
                             .join('')

    }

}

module.exports = Shutdown